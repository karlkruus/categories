import React, { Component } from 'react';
import './Category.css';

class Category extends Component {
    constructor() {
        super()
        // isCollapsed is used to keep track of whether the component is collapsed or not
        this.state = {
            isCollapsed: true
        }
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        // Switch between true and false for the isCollapsed variable
        this.setState(state => ({ 
            isCollapsed: !state.isCollapsed 
        }));
    }
    render() {
        const isCollapsed = this.state.isCollapsed;
        const categories = [];
        // If there are child categories and this element is not collapsed, add more Category elements
        // Otherwise show the empty categories array, rendering nothing
        if (this.props.categories && !isCollapsed) {
            this.props.categories.forEach((category, i) => {
                categories.push(<Category key={ i } name={ category.name } categories={ category.categories }/>);
            });
        }
        return (
            <div className="Category">
                <h2 onClick={this.handleClick}>{ this.props.name }</h2>
                { categories }
            </div>
        );
    }
}

export default Category;
