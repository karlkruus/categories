import React, { Component } from 'react';
import Category from './Category.js';

class App extends Component {
    constructor() {
        super();
        // The data variable is used to store the parsed JSON
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        // This function makes calls to the inputted URL, making a GET request and expecting a JSON result
        const getJSON = function(url, callback) {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'json';
            xhr.onload = function() {
                var status = xhr.status;
                if (status === 200) callback(null, xhr.response);
                else callback(status, xhr.response);
            };
            xhr.send();
        };
        
        // Get an item called 'categories' from local storage. It expects a JSON string and parses it into an object.
        const storageData = JSON.parse(localStorage.getItem('categories'));
        // Set the context, as it disappears in getJSON.
        const context = this;
        // If data wasn't stored in the localStorage, get it
        if (!storageData) {
            // This makes a call to the url, using a proxy to bypass CORS
            getJSON('https://cors-anywhere.herokuapp.com/https://lumav.net/dummy/categories.json',
                function(err, data) {
                    // If the result throws an error, throw an error in the console
                    if (err !== null) console.error('Something went wrong: ' + err);
                    else {
                        // Set the state to the newly aquired JSON data, causing React to rerender and show the data
                        context.setState({data});
                        // Store the result into localStorage, turning it into a string beforehand.
                        localStorage.setItem('categories', JSON.stringify(data));
                    }
                }
            );
        }
        // If the data was stored in localStorage, just set it to the state, causing a rerender
        else context.setState({data: storageData});
    }
    render() {
        const categories = [];

        // If the data exists, iterate through it and create a Category component for each element in the array
        if (this.state.data.length !== 0) {
            this.state.data.categories.forEach((category, i) => {
                categories.push(<Category key={ i } name={ category.name } categories={ category.categories }/>);
            });
        }

        return (
            <div className="App">
                {/* Render the elements in the array. If it's empty it renders nothing. */}
                { categories }
            </div>
        );
    }
}

export default App;
